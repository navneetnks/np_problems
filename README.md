# README #

In order to run the project, you need to install all depedencies/requirements mentioned in requirements.txt file 
You may create separate virtual environment for the project.

Installing the requirements (file requirements.txt file present in the project):

pip install -r requirements.txt

Call `execute()` method from the selected module to run the program.

Description of individual problem statement is in _docstring_ of respective module 


### What is this repository for? ###


The repository is for creaing tutorials for non-deterministic (NP) problems. 
As separate app for basic graph algorthim is also in plan.



### Who do I talk to? ###

In case of any query or suggestion, drop mail at navneet.nks@gmail.com