def longest_increasing_subsequence(input: list):
    """
    :param input: list of int
    :return: list of int : LIS elements
    """
    output = [1 for i in range(len(input))]
    solution_index = [_ for _ in range(len(input))]
    print(input)
    max_lis_count = 1
    max_lis_last_index = 0
    for i in range(1, len(input)):
        for j in range(i):
            if input[i] > input[j] and output[i] < output[j] + 1:
                output[i] = output[j] + 1
                solution_index[i] = j
                if max_lis_count < output[i]:
                    max_lis_count = output[i]
                    max_lis_last_index = i
    lis = list()
    next = max_lis_last_index
    while True:
        lis.append(input[next])
        if next==solution_index[next]:
            break
        next = solution_index[next]

    return lis[::-1]


if __name__ == '__main__':
    print(longest_increasing_subsequence([3, 4, -1, 0, 6, 2, 1]))
