"""
Largest Sum Contiguous Subarray
"""
import sys


def kadane_sum(num_array):
    max_sum = -sys.float_info.max
    sum_so_far=0
    for num in num_array:
        sum_so_far+=num
        if max_sum<sum_so_far:
            max_sum=sum_so_far
        if sum_so_far<0:
            sum_so_far=0
    return max_sum


if __name__ == '__main__':
    print(f"Largest Sum Contiguous Subarray={kadane_sum([-2, -3, 4, -1, -2, 1, 5, -3])}")
