"""
To run code, install python3 and execution  python common_problems/stair_count.py
"""
def stair_path_combination(n):
    if n == 1:
        return 1
    if n == 2:
        return 2
    return stair_path_combination(n - 2) + stair_path_combination(n - 1)


def main():
    ans = stair_path_combination(5)
    print(f"result={ans}")


if __name__ == "__main__":
    main()
