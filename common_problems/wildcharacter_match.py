def wild_character_match(word, pattern):
    p_len = len(pattern)
    word_len = len(word)
    mat = [[False for i in range(p_len + 1)] for j in range(word_len + 1)]
    mat[0][0] = True
    for i in range(1, p_len + 1):
        if pattern[i - 1] == "*":
            mat[0][i] = mat[0][i-1]
    for i in range(1, word_len + 1):
        for j in range(1, p_len + 1):
            if word[i - 1] == pattern[j - 1] or pattern[j - 1] == "?":
                mat[i][j] = mat[i - 1][j - 1]
            elif pattern[j - 1] == "*":
                mat[i][j] = mat[i - 1][j] or mat[i][j - 1]
    # print(mat)
    return mat[word_len][p_len]


if __name__ == '__main__':
    print(wild_character_match("xcyayylmza", "x?y*z"))
