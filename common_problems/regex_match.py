def wildcard(word, pattern):
    mat = [[False for x in range(len(pattern) + 1)] for y in range(len(word) + 1)]
    mat[0][0] = True
    for i in range(1, len(pattern)):
        if pattern[i - 1] == "*":
            mat[0][i] = mat[0][i - 2]

    for i in range(1, len(word) + 1):
        for j in range(1, len(pattern) + 1):
            if word[i - 1] == pattern[j - 1] or pattern[i - 1] == ".":
                mat[i][j] = mat[i - 1][j - 1]
            elif pattern[j - 1] == "*":
                mat[i][j] = mat[i][j - 2]
                if word[i - 1] == pattern[j - 2] or pattern[j - 2] == ".":
                    mat[i][j] = mat[i][j] or mat[i - 1][j]
    print(mat)
    return mat[len(word)][len(pattern)]


if __name__ == '__main__':
    print(wildcard("abbcdb", "ab*c.b"))
