"""
Value of Counter            Subset
    000                    -> Empty set
    001                    -> a
    010                    -> b
    011                    -> ab
    100                    -> c
    101                    -> ac
    110                    -> bc
    111                    -> abc
"""

def power_set(a):
    total = 1 << len(a)
    # print(f"total={total}")
    for i in range(0, total):
        for j in range(0, len(a)):
            # print(f"i={i}, j={j}")
            if (i >> j) & 1 != 0:
                print(f"{a[j]}", end=" ")
        print("")


if __name__ == '__main__':
    power_set(["a", "b", "c"])
