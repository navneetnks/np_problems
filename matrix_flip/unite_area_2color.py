
class Unite2ColorMatrix(object):
    """
    A matrix has elements in two color, i.e., black and white.
    An area comprise of same color elements in its neighbourhood.
    A person can pick a area and flip (change) its color (change the color of all its elements).
    Find minimum number of such flips required to make the complete matrix in a single color.

    """
    def __init__(self):
        self.matrix = [['B', 'w', 'B', 'w', 'B', 'w', 'B', 'w', 'B'],
                       ['w', 'w', 'w', 'w', 'B', 'w', 'w', 'w', 'w'],
                       ['B', 'w', 'B', 'B', 'B', 'B', 'B', 'w', 'B'],
                       ['w', 'w', 'B', 'B', 'B', 'B', 'B', 'w', 'w'],
                       ['B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'],
                       ['w', 'w', 'B', 'B', 'B', 'B', 'B', 'w', 'w'],
                       ['B', 'w', 'B', 'B', 'B', 'B', 'B', 'w', 'B'],
                       ['w', 'w', 'w', 'w', 'B', 'w', 'w', 'w', 'w'],
                       ['B', 'w', 'B', 'w', 'B', 'w', 'B', 'w', 'B']]

        self.row = 9
        self.column = 9

        self.connected_components = list()
        self.black_component_count = 0
        self.white_component_count = 0

    def get_neighbouring_nodes(self, row, col):
        neighbours_position = list()
        if col - 1 >= 0:
            neighbours_position.append((row, col - 1))
        if col + 1 < self.column:
            neighbours_position.append((row, col + 1))
        if row - 1 >= 0:
            neighbours_position.append((row - 1, col))
        if row + 1 < self.row:
            neighbours_position.append((row + 1, col))

        return neighbours_position

    def explore(self, row, column, color, visited=None):
        """
        Similar to exploring DFS with minor tweak

        """
        if not visited:
            visited = list()
        visited.append((row, column))
        neighbours = self.get_neighbouring_nodes(row, column)
        for neighbour in neighbours:
            item_row, item_col = neighbour
            if not neighbour in visited and self.matrix[item_row][item_col] == color:
                self.explore(item_row, item_col, color, visited)
        return visited

    def scan(self):
        """
        similar to finding connected components in a graph

        """
        visited = list()
        for row in range(0, self.row):
            for col in range(0, self.column):
                if not (row, col) in visited:
                    component = self.explore(row, col, self.matrix[row][col])
                    visited += component
                    self.connected_components.append(component)
                    if self.matrix[row][col] == 'B':
                        self.black_component_count += 1
                    else:
                        self.white_component_count += 1

    def find_min_flip(self):
        """
        keep flipping central item until it forms single connected component
        :return: int
        """
        self.scan()

        min_flip_component = self.black_component_count if self.black_component_count < self.white_component_count else self.white_component_count
        min_flip_counter = 0
        for i in range(0, min_flip_component):
            component, central_item_val = self.get_central_component()
            if central_item_val == 'B':
                new_val = 'w'
            else:
                new_val = 'B'
            self.flip_component(component, new_val)
            self.connected_components = list()
            self.black_component_count = 0
            self.white_component_count = 0
            self.scan()
            min_flip_counter += 1
            if self.black_component_count + self.white_component_count == 1:
                break

        if self.black_component_count + self.white_component_count == 1:
            return min_flip_counter
        else:
            return min_flip_component

    def flip_component(self, component, new_value):
        for item in component:
            row, col = item
            self.matrix[row][col] = new_value

    def get_central_component(self):
        central_item_pos = (self.row / 2, self.column / 2)
        central_item_val = self.matrix[self.row / 2][self.column / 2]
        for component in self.connected_components:
            if central_item_pos in component:
                # print "cenral component: " + str(component)
                # print "central_item_val:" + str(central_item_val)
                return component, central_item_val


# from matrix_flip.unite_area_2color import *
def execute():
    import pprint
    t = Unite2ColorMatrix()
    pp = pprint.PrettyPrinter(indent=4)
    print "color matrix:\n"
    pp.pprint(t.matrix)
    print "\nMinimum number of flips required to make the entire area in single color: {0}".format(t.find_min_flip())
